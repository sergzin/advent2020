import re


def day1():
    data = open("day1.txt").read()
    numbers = [int(x) for x in data.split('\n')]
    # print(numbers)
    for seq, num1 in enumerate(numbers):
        for num2 in numbers[seq:]:
            if num1 + num2 == 2020:
                # print(num1, num2)
                print(num1 * num2)

    for seq1, num1 in enumerate(numbers):
        for seq2, num2 in enumerate(numbers[seq1:]):
            for num3 in numbers[seq2:]:
                if num1 + num2 + num3 == 2020:
                    # print(num1, num2, num3)
                    print(num1 * num2 * num3)


def day2():
    data = open('day2.txt').read()
    p1 = 0
    p2 = 0
    for line in data.split('\n'):
        pol, pas = line.split(":")
        pas = pas.strip()
        low, hi = pol[:-1].split("-")
        sym = pol[-1:]
        low, hi = int(low), int(hi)
        v = pas.count(sym)
        if low <= v <= hi:
            # print(low, hi, sym, pas)
            p1 += 1
        if sum((pas[low - 1] == sym, pas[hi - 1] == sym)) == 1:
            # print(low-1, hi-1, sym, pas)
            p2 += 1
    print("Total p1: {}".format(p1))
    print("Total p2: {}".format(p2))


def day3():
    data = open("day3.txt").read()
    m = [[x for x in line] for line in data.split('\n')]
    width = len(m[0])
    hight = len(m)

    def run(sx, sy):
        x = 0
        trees = 0
        for y in range(0, hight, sy):
            if m[y][x] == '#':
                trees += 1
            x += sx
            if x >= width:
                x = x % width
        return trees

    r1 = run(1, 1)
    r2 = run(3, 1)
    r3 = run(5, 1)
    r4 = run(7, 1)
    r5 = run(1, 2)
    print(r1, r2, r3, r4, r5)
    print("Day3 mul", r1 * r2 * r3 * r4 * r5)


def day4():
    data = open("day4.txt").read()
    passports = re.split(r'\n\n', data)
    # main_fields = re.compile(
    #     r"(?=.*\bbyr:)(?=.*\biyr:)(?=.*\beyr:)(?=.*\bhgt:)(?=.*\bhcl:)(?=.*\becl:)(?=.*\bpid:).+",
    #     flags=re.DOTALL)
    main_fields = re.compile(
        r"(?=.*\bbyr:(?P<byr>\d{4})\b)(?=.*\biyr:(?P<iyr>\d{4})\b)(?=.*\beyr:(?P<eyr>\d{4})\b)(?=.*\bhgt:(?:(?P<hgtc>\d+)cm|(?P<hgti>\d+)in))(?=.*\bhcl:(?P<hcl>#[0-9a-z]{6})\b)(?=.*\becl:(?P<ecl>(?:amb|blu|brn|gry|grn|hzl|oth)\b))(?=.*\bpid:(?P<pid>\d{9})\b).+",
        flags=re.DOTALL)
    total = 0
    for batch in passports:
        m = main_fields.search(batch)
        if m:
            result = m.groupdict()
            # print(batch, '\n')
            byr = int(result['byr'])
            if not 1920 <= byr <= 2002:
                continue
            iyr = int(result['iyr'])
            if not 2010 <= iyr <= 2020:
                continue
            eyr = int(result['eyr'])
            if not 2020 <= eyr <= 2030:
                continue
            hgtcm = int(result['hgtc']) if result['hgtc'] else None
            hgtin = int(result['hgti']) if result['hgti'] else None
            if hgtcm and not 150 <= hgtcm <= 193:
                continue
            if hgtin and not 59 <= hgtin <= 76:
                continue
            total += 1
    print("Valid passes: {}".format(total))


def day5():
    data = open("day5.txt").read()
    m = data.split('\n')
    seats = {}
    for line in m:
        row, col = splitter(line)
        seat_id = row * 8 + col
        seats[seat_id] = row, col
    print("Length ", len(seats))
    print("Max seat id: ", max(seats))
    for my in range(1, 1022):
        if my in seats:
            continue
        if my - 1 in seats and my + 1 in seats:
            print("My seat id ", my)


def splitter(line):
    #  BFFFBBF_RRR
    row_str = line[:7]
    col_str = line[7:]
    row_str = row_str.replace('B', '1').replace('F', '0')
    col_str = col_str.replace('R', '1').replace('L', '0')
    return int(row_str, 2), int(col_str, 2)


def day6():
    data = open("day6.txt").read()
    groups = [g for g in data.split('\n\n')]
    print("Total p1: ", sum(map(lambda x: len(set(x.replace('\n', ''))), groups)))
    total = 0
    for group in groups:
        ind = [set(d) for d in group.split('\n')]
        common = set.intersection(*ind)
        total += len(common)
    print("Total p2: ", total)


def day7():
    class Bag:
        def __init__(self, shade, color):
            super(Bag, self).__init__()
            self.shade = shade
            self.color = color

        def __repr__(self):
            return "{}_{}".format(self.shade, self.color)

        def __hash__(self):
            return hash('{}_{}'.format(self.shade, self.color))

        def __eq__(self, other):
            return self.__repr__() == other.__repr__()

    with open("day7.txt") as fh:
        data = fh.read().splitlines()
    head = re.compile(r"^(\w+) (\w+) bags contain (.+)")
    tail = re.compile(r"(?:(\d+) (\w+) (\w+) (?:bag|bags)[,.])")
    my_bag = Bag('shiny', 'gold')
    counter = 0
    bag_list = [my_bag]
    while counter < len(bag_list):
        for line in data:
            m = head.search(line)
            if not m:
                print("Didnt match line!!! {}".format(line))
                continue
            bag = Bag(m.group(1), m.group(2))
            m2 = tail.findall(m.group(3))
            if not m2:
                continue
            for n, shade, color in m2:
                other_bag = Bag(shade, color)
                if bag_list[counter] == other_bag and bag not in bag_list:
                    bag_list.append(bag)
        counter += 1
    print(len(bag_list) - 1)


def day7_2():
    import re

    def integers(s):  # borrowed from Norvig's AoC ipynb
        return [int(i) for i in re.split(r'\D+', s) if i]

    def part_1(rules, start_bag):
        bags = [start_bag]
        counter = 0
        while counter < len(bags):
            for line in rules:
                if bags[counter] in line:
                    new_bag = ' '.join(line.split()[0:2])
                    if new_bag not in bags:
                        bags.append(new_bag)
            counter += 1
        return len(bags) - 1

    def part_2(rules, color):
        bags = {}
        for line in rules:
            colors = re.findall(r'(\w* \w*) bag', line)
            primary = colors[0]
            secondary = list(zip(colors[1:], integers(line)))
            bags[primary] = dict(secondary)

        def stack(bag):
            total = 1
            if bags[bag]:
                for inside in bags[bag]:
                    total += bags[bag][inside] * stack(inside)
                return total
            return total

        return stack(color) - 1

    def main():
        d = open('day7.txt').read().splitlines()
        print(part_1(d, 'shiny gold'))
        print(part_2(d, 'shiny gold'))

    main()


def day8():
    stack = []
    with open('day8.txt') as fh:
        for line in fh.read().splitlines():
            command, value = line.strip().split()
            stack.append((command, int(value)))

    def run_sim(data):
        acc = 0
        seen = set()
        idx = 0
        stack_len = len(data)
        while True:

            if 0 <= idx < stack_len:
                cmd, arg = data[idx]
            else:
                # print(f"Index {idx} out of range acc = {acc}")
                return 'done', acc

            # print(idx, cmd, arg, "acc: ", acc)

            if idx in seen:
                # print(f"Seen index {idx}, accumulator {acc}")
                return 'loop', acc
            seen.add(idx)

            if cmd == 'acc':
                acc += arg
                idx += 1
            elif cmd == 'nop':
                idx += 1
            elif cmd == 'jmp':
                idx += arg

    _, value = run_sim(stack)
    print(f"Result Part 1 {value}")

    for index, item in enumerate(stack):
        result = None
        cmd, arg = item
        if cmd == 'nop':
            new_stack = stack.copy()
            new_stack[index] = ('jmp', arg)
            result = run_sim(new_stack)
        elif cmd == 'jmp':
            new_stack = stack.copy()
            new_stack[index] = ('nop', arg)
            result = run_sim(new_stack)

        if result and result[0] == 'done':
            print(f"Result Part 2 {result[1]}")
            break


def day9():
    with open("day9.txt") as fh:
        data = [int(x) for x in fh.read().splitlines()]

    def check_sum(data, number):
        for idx, i in enumerate(data, start=1):
            for j in data[idx:]:
                # print(f'{i}, {j}')
                if i + j == number:
                    return f'{i}+{j}={number}'
    for index, n in enumerate(data[25:], start=25):
        preamble = data[index-25:index]
        r = check_sum(preamble, n)
        if not r:
            Number = n
    print(f'Part 1 N = {Number}')

    for idx, i in enumerate(range(len(data))):
        for j in range(i+1, len(data)):
            sub = data[i:j]
            subsum = sum(sub)
            if subsum > Number:
                # print(f"Subsum {subsum} > Number {Number}")
                break
            elif subsum == Number:
                if len(sub) > 1:
                    first = sorted(sub)[0]
                    last = sorted(sub)[-1]
                    print(f"Found solution for part 2 {first + last}")


def day10():
    with open("day10.txt") as fh:
        data = [int(x) for x in fh.read().splitlines()]
    data.append(0)
    data.append(max(data)+3)
    ordered = sorted(data)
    diff = [ordered[i]-ordered[i-1] for i in range(1, len(ordered))]
    c1 = diff.count(1)
    c3 = diff.count(3)
    print(f"Part1 c1+c3 = {c1*c3}")
    import collections
    combs = collections.defaultdict(int, {0: 1})  # really clever way from youtube
    for n in ordered[1:-1]:
        combs[n] = combs[n - 1] + combs[n - 2] + combs[n - 3]
    print(f"Part 2 {combs[ordered[-2]]}")


def day11():
    from numpy import array
    from pprint import pprint
    # 1 means empty
    # 0 means floor
    # -1 occupied
    data = []
    with open("day11.txt") as fh:
        data = [list(x) for x in fh.read().splitlines()]
        # for line in fh.read().splitlines():
            # row = [0 if x == '.' else 1 for x in line]
            # data.append(row)
    # y, x
    directions = ((-1, -1), (-1, 0), (-1, 1),
                  (0, -1),           (0, 1),
                  (1, -1), (1, 0), (1, 1))

    from copy import deepcopy

    def run_sim(a):
        newgen = deepcopy(a)
        colsize = len(a)
        changed = False
        for idy, row in enumerate(a):
            rowsize = len(row)
            for idx, j in enumerate(row):
                # print(idy, idx, j)
                if j == 'L':
                    neighbours = 0
                    for dy, dx in directions:
                        if 0 <= dy + idy < colsize and 0 <= dx + idx < rowsize:
                            neighbour = a[dy+idy][dx+idx]
                            if neighbour == '#':
                                neighbours += 1
                    if neighbours == 0:
                        newgen[idy][idx] = '#'
                        changed = True
                elif j == '#':
                    occupied = 0
                    for dy, dx in directions:
                        if 0 <= dy + idy < colsize and 0 <= dx + idx < rowsize:
                            neighbour = a[dy+idy][dx+idx]
                            if neighbour == '#':
                                occupied += 1
                    if occupied >= 4:
                        newgen[idy][idx] = 'L'
                        changed = True
                else:
                    newgen[idy][idx] = j
        return newgen, changed

    genx = deepcopy(data)
    n_of_sim = 0
    while True:
        # sim = ''.join([''.join(x)+'\n' for x in genx])+"\n\n"
        # print(sim)
        genx, changed_status = run_sim(genx)
        n_of_sim += 1
        if not changed_status:
            n_of_seats = sum([x.count('#') for x in genx])
            print(f'Generation N= {n_of_sim}, seats= {n_of_seats}')
            break



if __name__ == '__main__':
    # day1()
    # day2()
    # day3()
    # day4()
    # day5()
    # day6()
    # day7()
    # day7_2()
    # day8()
    # day9()
    # day10()
    day11()